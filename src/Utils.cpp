#include <cmath>
#include "Utils.hpp"

Coord::Coord() { };
Coord::Coord(float x_, float y_, float z_) {
    x = x_;
    y = y_;
    z = z_;
}

/** Simple lerp function */
float lerp(float a, float b, float t) {
    return a + (b - a) * t;
}

/** Simple lerp function for 3D coordinates */
Coord lerp3D(Coord a, Coord b, float t) {
    return Coord(lerp(a.x, b.x, t), lerp(a.y, b.y, t), lerp(a.z, b.z, t));
}

/** Returns the number of hexagons separating two hexagons on a coordinate plane */
int hex_dist(Coord a, Coord b) {
    int az = -a.x - a.z;
    int bz = -b.x - b.z;

    return std::fmax(std::fmax(std::abs(a.x - b.x), std::abs(a.z - b.z)), std::abs(az - bz));
}

/** 
    Rounds a 3D coordinate of floats to integers, ensuring that rounding follows the constraint of 
    axial coordinates
*/
Coord hex_round(Coord coord) {
    int rx = std::round(coord.x);
    int ry = std::round(coord.z); 
    int rz = std::round(-coord.x - coord.z);

    float x_diff = std::abs(rx - coord.x);
    float y_diff = std::abs(ry - coord.z);
    float z_diff = std::abs(rz - (-coord.x - coord.z));
   
    if (x_diff > y_diff && x_diff > z_diff) {
        rx = -ry - rz;
    } else if (y_diff > z_diff) {
        ry = -rx - rz;
    }

    return Coord(rx, std::round(coord.y), ry);
}

/** Converts hexagonal (axial) coordinates to regular cartesian grid coordinates */
Coord hex_to_grid(Coord coord) {
    float x = (std::sqrt(3)/2)*coord.x + std::sqrt(3)*coord.z;
    float z = 1.5*coord.x;

    return Coord(x, coord.y, z);
}

/** Converts cartesian grid coordinates to hexagonal (axial) coordinates */
Coord grid_to_hex(Coord coord) {
    float x = coord.z/1.5;
    float z = (1/std::sqrt(3))*coord.x - (1/3.0)*coord.z;

    return Coord(x, coord.y, z);
}
