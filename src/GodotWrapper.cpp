#include "Utils.hpp"
#include "HexCell.hpp"
#include "HexMap.hpp"

#include <Godot.hpp>
#include <Array.hpp>
#include <Dictionary.hpp>
#include <Node.hpp>
#include <Vector3.hpp>

/** These functions provide oft needed conversions.
 * godot::Vector3 <---> Coord
 * std::vector<Coord> ---> godot::Array w/ godot::Vector3 as elements
 */
inline godot::Vector3 convert(Coord c) {
    return godot::Vector3(c.x, c.y, c.z);
}

inline Coord convert(godot::Vector3 v) {
    return Coord(v.x, v.y, v.z);
}

inline godot::Array convert(std::vector<Coord> v) {
    godot::Array a;
    for (const Coord c: v) {
        a.push_back(convert(c));
    }
    return a;
}

/** This is just a wrapper whose interface mimics HexMap's. The only changes made here are type
 * changes, where functions return Godot types instead of standard library types (all conversions
 * are covered in the above conversion functions, and the addition of the _register_methods function,
 * which is necessitated by the Godot engine. While some functions may not need any conversion
 * overrides, all functions but _init, constructors, and destructors need to be included in
 * _register_methods.
 *
 * That being the case, please see include/HexMap.hpp for comments relevant to each function
 */
class GodotWrapper : public HexMap, public godot::Node {
    GODOT_CLASS(GodotWrapper, godot::Node);

    public:
        void set_cell(godot::Vector3 pos_, int terrain=1) { 
            HexMap::set_cell(pos_.x, pos_.y, pos_.z, terrain); 
        }
        void _init() { HexMap::_init(); }

        godot::Dictionary get_cell(godot::Vector3 pos_) {
            godot::Dictionary dict;
            HexCell cell = HexMap::get_cell(pos_.x, pos_.y, pos_.z);
            dict["position"] = convert(cell.position);
            dict["terrain"] = cell.TERRAIN_TYPE;

            return dict;
        }

        godot::Vector3 closest_vertical_cell(godot::Vector3 coord_, int depth_=5) {
            Coord coord = convert(coord_);
            return convert(HexMap::closest_vertical_cell(coord, depth_));
        }

        godot::Array line(godot::Vector3 a_, godot::Vector3 b_) {
            Coord a = convert(a_);
            Coord b = convert(b_);
            return convert(HexMap::line(a, b));
        }

        godot::Array ring(godot::Vector3 center_, int radius_=1) {
            Coord center = convert(center_); 
            return convert(HexMap::ring(center, radius_));
        }

        godot::Array path(godot::Vector3 start_, godot::Vector3 end_) {
            Coord start = convert(start_);
            Coord end = convert(end_);
            return convert(HexMap::path(start, end));
        }

        static void _register_methods() {
            godot::register_method("set_cell", &GodotWrapper::set_cell);
            godot::register_method("get_cell", &GodotWrapper::get_cell);
            godot::register_method("closest_vertical_cell", &GodotWrapper::closest_vertical_cell);
            godot::register_method("line", &GodotWrapper::line);
            godot::register_method("ring", &GodotWrapper::ring);
            godot::register_method("path", &GodotWrapper::path);
        }
};

/** GDNative Initialize **/
extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

/** GDNative Terminate **/
extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
}

/** NativeScript Initialize **/
extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);
    godot::register_tool_class<GodotWrapper>();
}
