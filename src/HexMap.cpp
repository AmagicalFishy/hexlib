#include <algorithm>
#include <iostream>
#include <map>
#include <queue>
#include <vector>

#include "Utils.hpp"
#include "HexCell.hpp"
#include "HexMap.hpp"

void HexMap::_init() { 
    for (int i=-HSIZE; i <= HSIZE; i++) {
        for (int j=-VSIZE; j <= VSIZE; j++) {
            for (int k=-HSIZE; k <= HSIZE; k++) {
                HexCell cell = HexCell(i, j, k);
                //cells.set(HexCell(i, j, k), i, j, k);
                cells.set(cell, i, j, k);
            }
        }
    }
}

HexMap::HexMap() { _init(); }

void HexMap::set_cell(int x, int y, int z, int terrain) {
    HexCell cell = HexCell(x, y, z, terrain);
    cells.set(cell, x, y, z);
}

HexCell& HexMap::get_cell(int x, int y, int z) {
    return cells(x, y, z);
}

Coord HexMap::closest_vertical_cell(Coord coord, int depth) {
    for (int i=0; i <= depth; ++i) {
        if (abs(coord.y) < VSIZE && cells(int(coord.x), int(coord.y) + i, int(coord.z)).TERRAIN_TYPE != 0) {
            return Coord(coord.x, coord.y + i, coord.z);
        } else if (abs(coord.y) < VSIZE && cells(int(coord.x), int(coord.y) - i, int(coord.z)).TERRAIN_TYPE != 0) {
            return Coord(coord.x, coord.y - i, coord.z);
        }
    }

    return Coord();
}

std::vector<Coord> HexMap::line(Coord a, Coord b) {
    int n = hex_dist(a, b);
    if (n > 0) { 
        std::vector<Coord> results;
        for (int i=0; i <= n; ++i) {
            Coord step = hex_round(lerp3D(a, b, float(1.0/n)*i));
            results.push_back(closest_vertical_cell(step));
        }
        
        return results;
    }
    return std::vector<Coord> { a };
}

std::vector<Coord> HexMap::ring(Coord center, int radius) { 
    std::vector<Coord> ring_coordinates;
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x, center.y, center.z + radius)));          // North
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x + radius, center.y, center.z)));          // North-East
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x + radius, center.y, center.z - radius))); // South-East
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x, center.y, center.z - radius)));          // South
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x - radius, center.y, center.z)));          // South-West
    ring_coordinates.push_back(closest_vertical_cell(Coord(center.x - radius, center.y, center.z + radius))); // North-West

    return ring_coordinates;
}

// Breadth First Search
std::vector<Coord> HexMap::path(Coord start, Coord finish) {
    HexCell* begin = &cells(start.x, start.y, start.z);
    HexCell* end = &cells(finish.x, finish.y, finish.z);

    std::queue<HexCell*> frontier; // Set up frontier
    frontier.push(begin); // Add start cell to frontier

    std::map<HexCell*, HexCell*> origin; // Set up path-tracing
    origin[begin] = begin;

    // Expand the frontier
    while (!frontier.empty()) {
        HexCell* current = frontier.front();
        frontier.pop();

        if (current == end) { // Break if we've reached the goal
            break;
        }

        for (int dir = Direction::N; dir != NumDirections; ++dir) {
            HexCell* neighbor = cells(current, dir);
            if (neighbor != current && neighbor->TERRAIN_TYPE > 0 && origin.find(neighbor) == origin.end()) {
                origin[neighbor] = current;
                frontier.push(neighbor);
            }
        }
    }

    // Put path in vector
    std::vector<Coord> path; 
    path.push_back(end->position);
    HexCell* step = origin[end];
    while (step != begin) {
        path.push_back(step->position);
        step = origin[step];
    }

    std::reverse(path.begin(), path.end());
    return path;
}

HexMap::~HexMap() { delete cells_; }
