#include "Utils.hpp"
#include "HexCell.hpp"

HexCell::HexCell() { TERRAIN_TYPE = 0; }
HexCell::HexCell(int x_, int y_, int z_, int terrain_type) {
    position = hex_to_grid(Coord(x_, y_, z_));

    TERRAIN_TYPE = terrain_type;
}

HexCell::HexCell(Coord coord, int terrain_type) {
    position = hex_to_grid(coord);
    TERRAIN_TYPE = terrain_type;
}

HexCell::HexCell(const HexCell& c) {
    position = c.position;
    TERRAIN_TYPE = c.TERRAIN_TYPE;
}

HexCell::~HexCell() { } 
