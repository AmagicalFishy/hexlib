#include <stdexcept>

#include "catch.hpp"
#include "Utils.hpp"

SCENARIO("Coord struct is created") {
    WHEN("Contsructor is used with ints") {
        Coord coord = Coord(1, 2, 3);

        THEN("The x, y, and z coordinates are appropriately set") {
            REQUIRE(coord.x == 1);
            REQUIRE(coord.y == 2);
            REQUIRE(coord.z == 3);
        }
    }
    AND_WHEN("Constructor is used with floats") {
        Coord coord = Coord(1.0, 2.0, 3.0);

        THEN("The x, y, and z coordinates are appropriately set") {
            REQUIRE(coord.x == 1.0);
            REQUIRE(coord.y == 2.0);
            REQUIRE(coord.z == 3.0);
        }
    }

    AND_WHEN("We compare two Coords") {
        Coord a = Coord(0, 1, 2);
        Coord b = Coord(0, 1, 2);
        Coord c = Coord(0, 0, 0);

        THEN("The comparison operator works as expected") {
            REQUIRE(a == b);
            REQUIRE(b == a);
            REQUIRE(c != a);
            REQUIRE(!(c == a));
        }
    }
}

SCENARIO("Distance between two hexagons needs to be found") {
    WHEN("Different hexagonal coordinates are input") {
        Coord a = Coord(0, 0, 0);
        Coord b = Coord(1, 0, 3);
        Coord c = Coord(0, 0, 4);
        Coord d = Coord(-1, 0, 3);
        
        THEN("The answer is correct") {
            REQUIRE(hex_dist(a, b) == 4);
            REQUIRE(hex_dist(a, c) == 4);
            REQUIRE(hex_dist(a, d) == 3);
        }
    }
}
