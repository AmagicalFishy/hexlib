#include <stdexcept>
#include <array>
#include <ctime>
#include <iostream>

#include "catch.hpp"
#include "Utils.hpp"
#include "HexCell.hpp"
#include "HexMap.hpp"

SCENARIO("Change random seed...") {
    srand(time(NULL));
    HexCell p = HexCell();
}

SCENARIO("Test HexMap Getter and Setter") {
    HexMap map;
    WHEN("HexMap()") {
        THEN("No errors") {
            REQUIRE(1==1);
        }
    }
    AND_WHEN("HexMap::set - Randomly set cells") {
        const int number = 30;
        std::array<Coord, number> coords;
        for (int i=0; i < number; ++i) {
            coords[i] = Coord((rand() % HSIZE*2) - HSIZE, (rand() % VSIZE) - VSIZE, (rand() % HSIZE) - HSIZE); 
            map.set(coords[i].x, coords[i].y, coords[i].z);
        }

        THEN("HexMap::get - Get cells that were set") {
            for (int i=0; i < number; ++i) {
                REQUIRE(map.get(coords[i].x, coords[i].y, coords[i].z).TERRAIN_TYPE == 1);
            }

        }

        AND_THEN("HexMap::get - Cells can be changed by reference") {
            HexCell& cell = map.get(coords[0].x, coords[0].y, coords[0].z);
            cell.TERRAIN_TYPE = 2;
            REQUIRE(map.get(coords[0].x, coords[0].y, coords[0].z).TERRAIN_TYPE == 2);
        }
    }
}

SCENARIO("HexMap::closest_vertical_neighbor") {
    HexMap map;
    const int number = VSIZE;
    std::array<int, number> heights;

    WHEN("Set pairs of cells to some random height") {
        for (int i=0; i < number; ++i) {
            heights[i] = (rand() % VSIZE*2) - VSIZE;
            map.set(i, heights[i], i); 
        }

        THEN("Ensure closest neighbor returns appropriately") {
            for (int i=0; i < number; ++i) { 
                REQUIRE(map.closest_vertical_cell(Coord(i, 0, i), VSIZE) == Coord(i, heights[i], i));
            }
        }
    }
}

SCENARIO("HexMap::line") {
    HexMap map;
    for (int i=-HSIZE; i <= HSIZE; ++i) {
        for (int j=-HSIZE; j <= HSIZE; ++j) {
            map.set(i, 0, j);
        }
    }

    WHEN("Lines drawn across the axial axes") {
        int len = (rand() % HSIZE) + 1;
        Coord a = Coord(-len, 0, 0);
        Coord b = Coord(len, 0, 0);
        Coord c = Coord(0, 0, -len);
        Coord d = Coord(0, 0, len);

        std::vector<Coord> x_line = map.line(a, b);
        std::vector<Coord> z_line = map.line(c, d);

        THEN("Function returns appropriately") {
            for (int i=-len; i < len; ++i) {
                REQUIRE(x_line[i + len] == Coord(i, 0, 0));
                REQUIRE(z_line[i + len] == Coord(0, 0, i));
            }
        }
    }
}

SCENARIO("HexMap::ring") {
    HexMap map;
    for (int i=-HSIZE; i <= HSIZE; ++i) {
        for (int j=-HSIZE; j <= HSIZE; ++j) {
            map.set(i, 0, j);
        }
    }

    WHEN("Check rings of radius 1") {
        const int number = 30;
        std::array<Coord, number> centers;
        std::array<std::vector<Coord>, number> rings;
        for (int i=0; i < number; ++i) {
            centers[i] = Coord((rand() % HSIZE + 1) - HSIZE, (rand() % 10) - 5, (rand() % HSIZE + 1) - HSIZE);
            rings[i] = map.ring(centers[i], 1);
        }

        THEN("Function returns appropriately") {
            for (int i=0; i < number; ++i) {
                REQUIRE(rings[i][0] == Coord(centers[i].x,      0, centers[i].z + 1));
                REQUIRE(rings[i][1] == Coord(centers[i].x + 1,  0, centers[i].z));
                REQUIRE(rings[i][2] == Coord(centers[i].x + 1,  0, centers[i].z - 1));
                REQUIRE(rings[i][3] == Coord(centers[i].x,      0, centers[i].z - 1));
                REQUIRE(rings[i][4] == Coord(centers[i].x - 1,  0, centers[i].z));
                REQUIRE(rings[i][5] == Coord(centers[i].x - 1,  0, centers[i].z + 1));
            }
        }
    }
}

SCENARIO("HexMap::path") { 
    HexMap map = HexMap();

    WHEN("Build simple path") {
        // Path tiles
        std::array<Coord, 13> path = {
            Coord(1, 0, 0),
            Coord(1, 0, 1),
            Coord(2, 0, 1),
            Coord(3, 0, 0),
            Coord(3, 0, -1),
            Coord(4, 0, -2),
            Coord(5, 0, -2),
            Coord(5, 0, -1),
            Coord(5, 0, 0),
            Coord(6, 0, 0),
            Coord(7, 0, -1),
            Coord(7, 0, -2),
            Coord(7, 0, -3),
        };

        for (const Coord coord: path) {
            map.set(coord.x, coord.y, coord.z);
        }

        // Boundary Tiles
        map.set(-1, 0, 0, -1);
        map.set(0, 0, -1, -1);
        map.set(1, 0, -1, -1);
        map.set(2, 0, -1, -1);
        map.set(2, 0, 0, -1);
        map.set(3, 0, 1, -1);
        map.set(3, 0, -2, -1);
        map.set(4, 0, -3, -1);
        map.set(5, 0, -3, -1);
        map.set(6, 0, -3, -1);
        map.set(6, 0, -2, -1);
        map.set(6, 0, -1, -1);
        map.set(7, 0, -4, -1);
        map.set(8, 0, -5, -1);
        map.set(9, 0, -5, -1);
        map.set(9, 0, -4, -1);
        map.set(8, 0, -3, -1);
        map.set(8, 0, -2, -1);
        map.set(8, 0, -1, -1);
        map.set(7, 0, 0, -1);
        map.set(6, 0, 1, -1);
        map.set(5, 0, 1, -1);
        map.set(4, 0, 1, -1);
        map.set(4, 0, 0, -1);
        map.set(4, 0, -1, -1);
        map.set(3, 0, 2, -1);
        map.set(2, 0, 2, -1);
        map.set(1, 0, 3, -1);
        map.set(1, 0, 2, -1);
        map.set(0, 0, 3, -1);
        map.set(0, 0, 2, -1);
        map.set(0, 0, 1, -1);
        map.set(-1, 0, 1, -1);

        // Beginning and End Coordinates
        Coord start = Coord(0, 0, 0);
        Coord end = Coord(8, 0, -4);
        map.set(start.x, start.y, start.z, 1);
        map.set(end.x, end.y, end.z, 1);

                
        THEN("Path is correct") {
            std::vector<Coord> pathfound = map.path(start, end);
            std::vector<Coord> shit;
            for (const Coord coord: path) {
                shit.push_back(hex_to_grid(coord));
            }

            for (int i=1; i < path.size() - 1; ++i) {
                REQUIRE(pathfound[i] == hex_to_grid(path[i]));
            }
        }
    }
}
