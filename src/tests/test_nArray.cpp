#include <stdexcept>
#include <array>

#include "catch.hpp"
#include "Utils.hpp"
#include "nArray.hpp"

SCENARIO("nArray - Fill") {
    nArray<int, 10, 10>* narray_ = new nArray<int, 10, 10>;
    nArray<int, 10, 10> narray = *narray_;

    WHEN("Initialize to max boundaries") {
        for (int i=-10; i <= 10; ++i) {
            for (int j=-10; j <= 10; ++j) {
                for (int k=-10; k <= 10; ++k) {
                    narray.set(5, i, j, k);
                }
            }
        }
    }

    THEN("No errors are thrown") {
        REQUIRE(1 == 1);
        delete narray_;
    }
}
SCENARIO("nArray::set & nArray::get") {
    nArray<int, 64, 16> narray;
   
    const int number = 50;
    std::array<std::pair<Coord, int>, number> storage;
    
    for (int i=0; i < number; ++i) {
        int x = (rand() % 128) - 64;
        int z = (rand() % 128) - 64;
        int y = (rand() % 32) - 15; // We save one for a later test
        storage[i] = std::make_pair(Coord(x, y, z), rand() % 100);
    } 

    WHEN("Set and get") {
        for (const std::pair<Coord, int> pair: storage) {
            narray.set(pair.second, pair.first.x, pair.first.y, pair.first.z);
        }

        THEN("No error is thrown") {;
            for (const std::pair<Coord, int> pair: storage) {
                REQUIRE(narray(pair.first.x, pair.first.y, pair.first.z) == pair.second);
            }
        }
    }

    WHEN("Set - Out of bounds") {
        THEN("An error is thrown") {
            REQUIRE_THROWS_AS(narray.set(1, -257, 1, 1), std::out_of_range);
        }
    }
    WHEN("Get - Out of bounds") {
        THEN("An error is thrown") {
            REQUIRE_THROWS_AS(narray(-257, 1, 1), std::out_of_range);
        }
    }

    WHEN("Get element that hasn't been set") {
        THEN("A default-constructed type is returned") {
            REQUIRE(narray(0, -16, 0) == 0);
        }
    }
}

SCENARIO("nArray::operator() - Direction Check") {
    nArray<int, 64, 16> narray;

    WHEN("Set single hex and its neighbors") {
        std::array<Coord, 21> cells = {
            // This Level
            Coord(0, 0, 0), // Base
            Coord(0, 0, 1), // N
            Coord(1, 0, 0), // NE
            Coord(1, 0, -1), // SE
            Coord(0, 0, -1), // S
            Coord(-1, 0, 0), // SW
            Coord(-1, 0, 1), // NW

            // Up One Level
            Coord(0, 1, 0),
            Coord(0, 1, 1),
            Coord(1, 1, 0),
            Coord(1, 1, -1),
            Coord(0, 1, -1),
            Coord(-1, 1, 0),
            Coord(-1, 1, 1),

            // Down One Level
            Coord(0, -1, 0),
            Coord(0, -1, 1),
            Coord(1, -1, 0),
            Coord(1, -1, -1),
            Coord(0, -1, -1),
            Coord(-1, -1, 0),
            Coord(-1, -1, 1)
        };

        for (const Coord cell: cells) {
            narray.set(100, cell.x, cell.y, cell.z);
        }
        
        THEN("Neighbors return appropriately") {
            // Sanity Checks
            REQUIRE(narray(1, 1, 1) == 0);
            REQUIRE(narray(-1, -1, -1) == 0);
            REQUIRE(narray(0, 0, 0) == 100);

            // Ensure neighbors are appropriately fetched
            for (int dir = Direction::N; dir != NumDirections; ++dir) {
                REQUIRE(narray(0, 0, 0, dir) == 100);
            }

            // More Sanity Checks
            int number = 50;
            int x = 0;
            int y = 0;
            for (int i=0; i < number; ++i) {
                while ((x >= -1 && x <= 1) && (y >= -1 && y <= 1)) {
                    x = (rand() % 128) - 64;
                    y = (rand() % 128) - 64;
                }
                REQUIRE(narray(x, 0, y) != 100);

            }
        }
    }
}

SCENARIO("nArray::operator() - Pointer Direction Check") {
    nArray<int, 64, 16> narray;

    WHEN("Set single hex and its neighbors") {
        std::array<Coord, 21> cells = {
            // This Level
            Coord(0, 0, 0), // Base
            Coord(0, 0, 1), // N
            Coord(1, 0, 0), // NE
            Coord(1, 0, -1), // SE
            Coord(0, 0, -1), // S
            Coord(-1, 0, 0), // SW
            Coord(-1, 0, 1), // NW

            // Up One Level
            Coord(0, 1, 0),
            Coord(0, 1, 1),
            Coord(1, 1, 0),
            Coord(1, 1, -1),
            Coord(0, 1, -1),
            Coord(-1, 1, 0),
            Coord(-1, 1, 1),

            // Down One Level
            Coord(0, -1, 0),
            Coord(0, -1, 1),
            Coord(1, -1, 0),
            Coord(1, -1, -1),
            Coord(0, -1, -1),
            Coord(-1, -1, 0),
            Coord(-1, -1, 1)
        };

        for (const Coord cell: cells) {
            narray.set(100, cell.x, cell.y, cell.z);
        }
        
        THEN("Neighbors return appropriately") {
            // Sanity Checks
            REQUIRE(narray(1, 1, 1) == 0);
            REQUIRE(narray(-1, -1, -1) == 0);
            REQUIRE(narray(0, 0, 0) == 100);

            // Ensure neighbors are appropriately fetched
            for (int dir = Direction::N; dir != NumDirections; ++dir) {
                REQUIRE(*(narray(&narray(0, 0, 0), dir)) == 100);
            }
            
            // More Sanity Checks
            int number = 50;
            int x = 0;
            int y = 0;
            for (int i=0; i < number; ++i) {
                while ((x >= -1 && x <= 1) && (y >= -1 && y <= 1)) {
                    x = (rand() % 64) - 32;
                    y = (rand() % 64) - 32;
                }
                REQUIRE(narray(x, 0, y) != 100);

            }
        }
    }

    AND_WHEN("nArray::(T*, dir) Operation on object outside of array") {
        int i = 36;
        THEN("Error is triggered") {
            REQUIRE_THROWS_AS(narray(&i, Direction::N), std::out_of_range);
        }
    }
}
