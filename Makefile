#*************************************
# Makefile for Godot library: HexLib #
#*************************************
#********************
# VARS 
###
CXX = g++
CXXFLAGS = -Wall \
		   -Iinclude \
		   -Igodot-cpp/include \
		   -Igodot-cpp/include/core \
		   -Igodot-cpp/include/gen \
		   -Igodot-cpp/godot_headers
SRC = Utils HexCell HexMap GodotWrapper
SRCDIR = src/
OBJECTS = $(addsuffix .o, $(addprefix $(SRCDIR), $(SRC)))

TEST = test_nArray test_Utils test_HexMap main
TESTDIR = src/tests/
TESTS = $(addsuffix .o, $(addprefix $(TESTDIR), $(TEST)))

#********************
# RULES
###
build: $(OBJECTS)
	$(CXX) -o bin/x11/libHexLib.so -shared -rdynamic -g3 $^ -Lgodot-cpp/bin -lgodot-cpp.linux.debug.64

test: $(OBJECTS) $(TESTS)
	rm -f run_tests
	$(CXX) $(CXXFLAGS) $^ -rdynamic -g3 -o run_tests -Lgodot-cpp/bin -lgodot-cpp.linux.debug.64
	./run_tests

#********************
# Primary Files
###
.SECONDEXPANSION:
$(OBJECTS): $$(addsuffix .cpp, $$*)
	$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $(addsuffix .cpp, $*) -g3 -O3

#********************
# Test Files
###
.SECONDEXPANSION:
$(TESTS): $$(addsuffix .cpp, $$*)
	$(CXX) $(CXXFLAGS) -o $@ -c $(addsuffix .cpp, $*) -g3

#********************
# Other
###
clean: 
	rm -f src/*.o
	rm -f src/tests/*.o
	rm -f bin/x11/libHexLib.so
