#ifndef NARRAY
#define NARRAY

#include <iostream>
#include <array>
#include <stdexcept>
#include <string>

#include "Utils.hpp"

/** 
 * 3D array that allows for negative integers. All indices passed to the () operator are scaled by 
 * HSIZE and VSIZE respectively, so the bounds of the indices are, respectively, 
 * [-HSIZE, HSIZE] and [-VSIZE, VSIZE]
 *
 * @param T Object type being stored
 * @param HSIZE Half of the horizontal size of the array (defines the limit of both the 
 *        X and Z values) 
 * @param VSIZE Half of the vertical size of the array (defines the limit of the Y values)
 */
template <typename T, int HSIZE, int VSIZE> struct nArray { 
    private:
        std::array<T, (HSIZE*2 + 1)*(VSIZE*2 + 1)*(HSIZE*2 + 1)> narray;

        /** 
         * Checks whether or not the index is in bounds. std::array.at() will check whether the 
         * arithmetic of all requested indices is in bounds, but this checks whether or not each
         * individual index is within bounds
         */
        void bounds_check_(int x, int y, int z) {
            if (abs(x) > HSIZE || abs(y) > VSIZE || abs(z) > HSIZE) {
                const std::string indices = 
                    "(" + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + ")";
                throw std::out_of_range("One of your indices is out of range: " + indices);
            }
        }

    public:
        const int hz_ = HSIZE*2 + 1; // This just makes the code cleaner

        /* Iterators */
        auto begin() {
            return narray.begin();
        }

        auto end() {
            return narray.end();
        }

        /**
         * Returns element in array, scaled appropriately (e.g. - If HSIZE = VSIZE = 50, then
         * narray(-50, -50, -50) is scaled to (0, 0, 0), giving narray[0]
         */
        T& operator() (int x, int y, int z) {
            bounds_check_(x, y, z);
            return narray.at((x + HSIZE) + (z + HSIZE)*hz_ + (y + VSIZE)*hz_*hz_);
        }

        T& operator() (int x, int y, int z, int dir) { // Return Neighbors
            switch(dir) {
                // Same Level
                case Direction::N:
                    return narray.at((x + HSIZE) + (z + HSIZE + 1)*hz_ + (y + VSIZE)*hz_*hz_);

                case Direction::NE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE)*hz_ + (y + VSIZE)*hz_*hz_);

                case Direction::SE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE - 1)*hz_ + (y + VSIZE)*hz_*hz_);

                case Direction::S:
                    return narray.at((x + HSIZE) + (z + HSIZE - 1)*hz_ + (y + VSIZE)*hz_*hz_);

                case Direction::SW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE)*hz_ + (y + VSIZE)*hz_*hz_);

                case Direction::NW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE + 1)*hz_ + (y + VSIZE)*hz_*hz_);

                // Up One Level
                case Direction::U:
                    return narray.at((x + HSIZE) + (z + HSIZE)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::UN:
                    return narray.at((x + HSIZE) + (z + HSIZE + 1)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::UNE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::USE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE - 1)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::US:
                    return narray.at((x + HSIZE) + (z + HSIZE - 1)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::USW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                case Direction::UNW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE + 1)*hz_ + (y + VSIZE + 1)*hz_*hz_);

                // Down One Level
                case Direction::D:
                    return narray.at((x + HSIZE) + (z + HSIZE)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DN:
                    return narray.at((x + HSIZE) + (z + HSIZE + 1)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DNE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DSE:
                    return narray.at((x + HSIZE + 1) + (z + HSIZE - 1)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DS:
                    return narray.at((x + HSIZE) + (z + HSIZE - 1)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DSW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE)*hz_ + (y + VSIZE - 1)*hz_*hz_);

                case Direction::DNW:
                    return narray.at((x + HSIZE - 1) + (z + HSIZE + 1)*hz_ + (y + VSIZE - 1)*hz_*hz_);
            }

            return narray.at(0);
        }

        T* operator() (T* p, int dir) { // Return Neighbors
            // Ensure that provided pointer is within array
            T* begin = narray.begin();
            T* end = narray.end();
            if (!(std::equal_to<T*>()(begin, p) || std::less<T*>()(begin, p)) && std::less<T*>()(p, end)) {
                throw std::out_of_range("Please provide a pointer which points to an object inside of the array");
            }

            T* pObj;

            switch(dir) {
                // Same Level
                case Direction::N:
                    pObj = p + hz_;
                    break;

                case Direction::NE:
                    pObj = p + 1;
                    break;

                case Direction::SE:
                    pObj = p + 1 - hz_;
                    break;

                case Direction::S:
                    pObj = p - hz_;
                    break;

                case Direction::SW:
                    pObj = p - 1;
                    break;

                case Direction::NW:
                    pObj = p - 1 + hz_;
                    break;

                // Up One Level
                case Direction::U:
                    pObj = p + hz_*hz_;
                    break;

                case Direction::UN:
                    pObj = p + hz_ + hz_*hz_;
                    break;

                case Direction::UNE:
                    pObj = p + 1 + hz_*hz_;
                    break;

                case Direction::USE:
                    pObj = p + 1 - hz_ + hz_*hz_;
                    break;

                case Direction::US:
                    pObj = p - hz_ + hz_*hz_;
                    break;

                case Direction::USW:
                    pObj = p - 1 + hz_*hz_;
                    break;

                case Direction::UNW:
                    pObj = p - 1 + hz_ + hz_*hz_;
                    break;

                // Down One Level
                case Direction::D:
                    pObj = p - hz_*hz_;
                    break;

                case Direction::DN:
                    pObj = p + hz_ - hz_*hz_;
                    break;

                case Direction::DNE:
                    pObj = p + 1 - hz_*hz_;
                    break;

                case Direction::DSE:
                    pObj = p + 1 - hz_ - hz_*hz_;
                    break;

                case Direction::DS:
                    pObj = p - hz_ - hz_*hz_;
                    break;

                case Direction::DSW:
                    pObj = p - 1 - hz_*hz_;
                    break;

                case Direction::DNW:
                    pObj = p - 1 + hz_ - hz_*hz_;
                    break;
            }

            if (!(std::equal_to<T*>()(begin, pObj) || std::less<T*>()(begin, pObj)) && std::less<T*>()(pObj, end)) {
                return p;
            }

            return pObj;
        }

        /** Return element in array according to regular array index */
        T& operator[] (int i) {
            return narray.at(i);
        }
       
        /** Set object w/ 3D indices */ 
        void set(const T object, int x, int y, int z) {
            bounds_check_(x, y, z);
            narray.at((x + HSIZE) + (z + HSIZE)*hz_ + (y + VSIZE)*hz_*hz_) = object;
        }
};

#endif
