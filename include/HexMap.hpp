#ifndef HEXMAP
#define HEXMAP

#include <vector>

#include "Utils.hpp"
#include "nArray.hpp"
#include "HexCell.hpp"

const int HSIZE = 64; // 128 on each side of 0
const int VSIZE = 16; // 32 on each side of 0

class HexMap {

    protected: 
        nArray<HexCell, HSIZE, VSIZE>* cells_ = new nArray<HexCell, HSIZE, VSIZE>;
        nArray<HexCell, HSIZE, VSIZE>& cells = *cells_;


    public:
        /** All method arguments should be in Axial coordinates; the HexCell class keeps a copy of
         * its cartesian coordinates while a particular HexCell's index in the nArray refers to
         * its Axial coordinates
         */

        HexMap();
        void _init(); // Called by Godot; we use it simply as a replacemet for the default constructor

        /** Getter & Setter methods for the internal array of HexCells
         *
         * @params x, y, z Coordinates of the cell to set/get
         */
        void set_cell(int x, int y, int z, int terrain=1);
        HexCell& get_cell(int x, int y, int z);

        /**
            Returns the coordinates of the closest vertical neighbor. This searches "depth" spaces up, 
            "depth" space down, and if nothing is found, returns a generic default constructed vecotor

            @param coord The coordinate of the tile whose neighbors will be fetched
            @return A set of coordinates of the nearest neighbor
        */
        Coord closest_vertical_cell(Coord coord, int depth=5);

        /**
            Returns a vector of coordinates of hexagons constructing the line between one hexagon 
            and another

            @param a Coordinate of the first hexagon from which the line starts
            @param b Coordinate of the second hexagon at which the line ends
        */
        std::vector<Coord> line(Coord a, Coord b);
        
        /**
            Returns a vector of coordinates forming a ring around the given tile's coordinates with
            given radius; the coordinates returned here are only the vertices of the ring (which has
            a hexagonal shape). Use HexMap::line to draw the lines between each vertex.

            @param coord The coordinate of the center tile around which the ring is formed
            @param radius The radius of the ring
        */
        std::vector<Coord>  ring(Coord center, int radius=1);

        std::vector<Coord> path(Coord start, Coord finish);
        ~HexMap();
};

#endif
