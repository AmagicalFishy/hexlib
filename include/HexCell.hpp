#ifndef HEXCELL
#define HEXCELL

#include <array>
#include "Utils.hpp"

/** Structure which represents a single hexagonal-prism */
struct HexCell {
    /** These store position in cartesian coordinates */
    Coord position;
    int TERRAIN_TYPE;

    HexCell();
    HexCell(int x_, int y_, int z_, int terrain_type=0);
    HexCell(Coord coord, int terrain_type=0);
    HexCell(const HexCell& c);
    ~HexCell();
};

#endif
