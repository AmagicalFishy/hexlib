#ifndef UTILS
#define UTILS

#include <array>
#include <cmath>
#include <iostream>

/** Represents direction */
enum Direction {
            N = 0,
            NE,
            SE,
            S,
            SW,
            NW,
            U,
            UN,
            UNE,
            USE,
            US,
            USW,
            UNW,
            D,
            DN,
            DNE,
            DSE,
            DS,
            DSW,
            DNW,
            NumDirections
};

/** Represents a coordinate */
struct Coord {
    float x;
    float y;
    float z;

    Coord();
    Coord(float x_, float y_, float z_);

    /** Comparison Operators */
    friend inline bool operator==(const Coord& lhs, const Coord& rhs) {
        if (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z) {
            return true;
        }
        return false;
    }

    friend inline bool operator!=(const Coord& lhs, const Coord& rhs) {
        return !(lhs == rhs);
    }

    friend inline std::ostream& operator<<(std::ostream& os, const Coord& coord) {
        os << "(" << coord.x << ", " << coord.y << ", " << coord.z << ")";
        return os;
    }

};

/** Simple lerp function for hexagonal coordinates */
Coord lerp3D(Coord a, Coord b, float t);

/** Returns the number of hexagons separating two hexagons on a 2D coordinate plane */
int hex_dist(Coord a, Coord b);

/** 
    Rounds coordinates of floats to integers, ensuring that rounding follows the constraint of 
    axial coordinates
*/
Coord hex_round(Coord coord);

/** Converts hexagonal (axial) coordinates to regular cartesian grid coordinates */
Coord hex_to_grid(Coord a);

#endif
