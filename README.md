HEX MAP
=======
- [ ] Hex Line with closest neighbor and w.o. closest neighbor
- [ ] Split into pathfinding + rendering
- [ ] Generate Voronoi diagrams to construct a navigation mesh
- [ ] Implement flower:

func flower(center, dict, k=1):
	# Break condition; once function reaches maximum resolution (one cell), add this cell to the
	# passed in dictionary
	if k == 0:
		dict[center] = 0
		return

	# Find the centers of the six smaller hexagonal-flowers
	# For even numbered iterations, the five surrounding shapes need to be offset
	var diam = pow(7, (k-1)/2)
	var offset = 0

	if k % 2 == 0:
		diam = 2*pow(7, (k/2 - 1))
		offset = diam/2

	var petal_centers = [
		Vector2(center.x, center.y),                                # Origin
		Vector2(center.x + diam, center.y + offset),                # NE
		Vector2(center.x + diam + offset, center.y - diam),         # SE
		Vector2(center.x + offset, center.y - diam - offset),       # S
		Vector2(center.x - diam, center.y - offset),                # SW
		Vector2(center.x - diam - offset, center.y + diam),         # NW
		Vector2(center.x - offset, center.y + diam + offset)        # N
	]

	for coord in petal_centers:
		flower(coord, dict, k-1)

NARRAY
======
- [ ] Make nArray specific to HexCell
- [ ] Begin nArray at (0, 0, 0) and only allow positive values, forming a square (like that Hex Map
      builder online)

HEXCELL
=======
- [ ] Make default terrain 1 - Walkable, 0 - Non-Walkable, -1 - Default

BUGS
====
- [ ] HexMap::line, HexMap::ring, etc. do not check if their return values are out of range (i.e. - 
      HexMap::ring may try to return something that's beyond the current array).

PATHFINDING
===========
- [ ] Dijkstra's algorithm to cause increased movement cost for being close to enemies, or increased
      cost for height values

FORMATIONS
==========
- [ ] A "Formation" is a graph where vertices are the units and edges are the spatial relationship
      between them. In flocking behavior, each unit attempts to stay in formation (perhaps: Planar
      Straight Line graph) by constantly moving towards their position in the graph
- [ ] When the formation moves, just translate the whole graph. When the formation rotates, just
      rotate the whole graph
- [ ] When units in a formation are moved (i.e. - translation) create pathfinding for just one of 
      the units, then, for every other unit, translate each vertex in the result by its place in 
      the formation. The unit's own flocking behavior pushes it in the direction of its place in
      the formation, but, ultimately, he wants to move towards the path.
        - [ ] What if the translated path is inaccessible?
            - [x] What if, in correcting the inaccessible-translated-path, the unit impedes on some
                  other unit's goal?
                      A: We can either shift everyone in the path to accomodate the new unit's goal
                         or we can just say that, if a unit has that spot as a goal, taht spot is 
                         inaccessible


